package pl.edu.pwsztar;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class ShoppingCartTest {

    private ShoppingCart shoppingCart = new ShoppingCart();

    @Test
    void shouldAddProductToCart() {
        boolean result = shoppingCart.addProducts("test", 2, 2);

        assertTrue(result);
    }

    @Test
    void shouldAddTwoSameProducts() {
        boolean firstResult = shoppingCart.addProducts("test", 2,2);
        boolean secondResult = shoppingCart.addProducts("test", 2,2);

        assertTrue(firstResult);
        assertTrue(secondResult);
    }

    @Test
    void shouldNotAddProductIfQuantityIsBelowZero() {
        boolean result = shoppingCart.addProducts("test", 2, -1);

        assertFalse(result);
    }

    @Test
    void shouldNotAddProductIfPriceIsBelowZero() {
        boolean result = shoppingCart.addProducts("test",-1, 2);

        assertFalse(result);
    }

    @Test
    void shouldNotAddProductIfQuantityIsEqualZero() {
        boolean result = shoppingCart.addProducts("test", 2, 0);

        assertFalse(result);
    }

    @Test
    void shouldNotAddProductIfPriceIsEqualZero() {
        boolean result = shoppingCart.addProducts("test", 0, 2);

        assertFalse(result);
    }

    @Test
    void shouldNotAddProductIfCartIsFull() {
        boolean result = shoppingCart.addProducts("test", 1, 500);

        assertFalse(result);
    }

    @Test
    void shouldDeleteProductIfExists() {
        shoppingCart.addProducts("test", 2, 2);
        boolean result = shoppingCart.deleteProducts("test", 2);

        assertTrue(result);
    }

    @Test
    void shouldNotDeleteProductIfQuantityToDeleteIsLessThanQuantityInCart() {
        shoppingCart.addProducts("test", 2, 2);
        boolean result = shoppingCart.deleteProducts("test", 5);

        assertFalse(result);
    }

    @Test
    void shouldNotDeleteProductIfQuantityIsBelowZero() {
        shoppingCart.addProducts("test", 2, 2);
        boolean result = shoppingCart.deleteProducts("test", -1);

        assertFalse(result);
    }

    @Test
    void shouldNotDeleteProductIfProductDoesNotExist() {
        boolean result = shoppingCart.deleteProducts("test", 1);

        assertFalse(result);
    }

    @Test
    void shouldReturnQuantityOfProducts() {
        int quantity = 2;
        shoppingCart.addProducts("test", 2, quantity);
        int quantityInCart = shoppingCart.getQuantityOfProduct("test");

        assertEquals(quantity, quantityInCart);
    }

    @Test
    void shouldReturnZeroIfProductDoesNotExist() {
        int quantityInCart = shoppingCart.getQuantityOfProduct("test");

        assertEquals(0, quantityInCart);
    }

    @Test
    void shouldReturnSumOfPrices() {
        shoppingCart.addProducts("test", 2, 2);
        shoppingCart.addProducts("test", 2, 2);
        int sumOfPrices = shoppingCart.getSumProductsPrices();

        assertEquals(8, sumOfPrices);
    }

    @Test
    void shouldReturnZeroIfCartIsEmpty() {
        int sumOfPrices = shoppingCart.getSumProductsPrices();

        assertEquals(0, sumOfPrices);
    }

    @Test
    void shouldReturnPriceIfProductExist() {
        String productName = "test";
        int productPrice = 2;
        shoppingCart.addProducts(productName, productPrice, 2);
        int price = shoppingCart.getProductPrice(productName);

        assertEquals(productPrice, price);
    }

    @Test
    void shouldReturnZeroPriceIfProductDoesNotExist() {
        int price = shoppingCart.getProductPrice("test");

        assertEquals(0, price);
    }

    @Test
    void shouldReturnAllProductNames() {
        String[] productNames = {"test1", "test2"};
        shoppingCart.addProducts(productNames[0], 2, 2);
        shoppingCart.addProducts(productNames[1], 2, 2);
        String[] returnedProductNames = shoppingCart.getProductsNames().toArray(new String[0]);

        for (int i = 0; i < productNames.length; i++) {
            assertEquals(productNames[i], returnedProductNames[i]);
        }
    }

    @Test
    void shouldReturnEmptyListIfCartEmpty() {
        List<String> productNames = shoppingCart.getProductsNames();
        List<String> emptyList = new ArrayList<String>();

        assertEquals(emptyList, productNames);
    }
}
