package pl.edu.pwsztar.repository.impl;

import pl.edu.pwsztar.entity.Product;
import pl.edu.pwsztar.repository.ShoppingCartRepository;

import java.util.Collection;
import java.util.HashMap;
import java.util.Optional;

public class ShoppingCartRepositoryImpl implements ShoppingCartRepository {

    private HashMap<String, Product> products = new HashMap<>();

    @Override
    public void addProduct(Product product) {
        products.put(product.getProductName(), product);
    }

    @Override
    public boolean deleteProduct(String productName) {
        return products.remove(productName) != null;
    }

    @Override
    public Optional<Product> getProductByName(String productName) {
        return Optional.ofNullable(products.get(productName));
    }

    @Override
    public Collection<Product> getAll() {
        return products.values();
    }
}
