package pl.edu.pwsztar.repository;

import pl.edu.pwsztar.entity.Product;

import java.util.Collection;
import java.util.Optional;

public interface ShoppingCartRepository {

    void addProduct(Product product);

    boolean deleteProduct(String productName);

    Optional<Product> getProductByName(String productName);

    Collection<Product> getAll();
}
