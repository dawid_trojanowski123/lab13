package pl.edu.pwsztar;

import pl.edu.pwsztar.entity.Product;
import pl.edu.pwsztar.repository.ShoppingCartRepository;
import pl.edu.pwsztar.repository.impl.ShoppingCartRepositoryImpl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class ShoppingCart implements ShoppingCartOperation {

    private ShoppingCartRepository shoppingCartRepository = new ShoppingCartRepositoryImpl();

    public boolean addProducts(String productName, int price, int quantity) {
        if (quantity <= 0 ||
                price <= 0 ||
                quantityOfAllProducts() >= ShoppingCartOperation.PRODUCTS_LIMIT ||
                quantity >= ShoppingCartOperation.PRODUCTS_LIMIT) {
            return false;
        }

        Optional<Product> optionalProduct = shoppingCartRepository.getProductByName(productName);
        if (optionalProduct.isPresent()) {
            Product presentProduct = optionalProduct.get();
            if (presentProduct.getPrice() == price) {
                Product newProduct = new Product(productName, quantity + presentProduct.getQuantity(), price);
                shoppingCartRepository.addProduct(newProduct);

                return true;
            } else {
                return false;
            }
        } else {
            Product newProduct = new Product(productName, price, quantity);
            shoppingCartRepository.addProduct(newProduct);

            return true;
        }
    }

    public boolean deleteProducts(String productName, int quantity) {
        int quantityOfProduct = getQuantityOfProduct(productName);
        if (quantity <= 0 || quantityOfProduct == 0 || quantity > quantityOfProduct) {
            return false;
        }

        if (quantity < quantityOfProduct) {
            Optional<Product> optionalProduct = shoppingCartRepository.getProductByName(productName);
            if (optionalProduct.isPresent()) {
                Product presentProduct = optionalProduct.get();
                Product newProduct = new Product(productName, presentProduct.getQuantity() - quantity, presentProduct.getPrice());
                shoppingCartRepository.deleteProduct(productName);
                shoppingCartRepository.addProduct(newProduct);

                return true;
            } else {
                return false;
            }
        } else {
            return shoppingCartRepository.deleteProduct(productName);
        }
    }

    public int getQuantityOfProduct(String productName) {
        return shoppingCartRepository
                .getProductByName(productName)
                .map(Product::getQuantity)
                .orElse(0);
    }

    public int getSumProductsPrices() {
        return shoppingCartRepository
                .getAll()
                .stream()
                .mapToInt( product -> product.getPrice() * product.getQuantity())
                .sum();
    }

    public int getProductPrice(String productName) {
        return shoppingCartRepository
                .getProductByName(productName)
                .map(Product::getPrice)
                .orElse(0);
    }

    public List<String> getProductsNames() {
        return shoppingCartRepository
                .getAll()
                .stream()
                .map(Product::getProductName)
                .sorted()
                .collect(Collectors.toList());
    }

    private int quantityOfAllProducts() {
        return shoppingCartRepository
                .getAll()
                .stream()
                .mapToInt(Product::getQuantity)
                .sum();
    }
}
